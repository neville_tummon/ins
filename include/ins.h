#ifndef _INS_H_
#define _INS_H_
#include "matrix.h"
#include <iostream>
#include "quat.h"
#include <math.h>

template <class T>
class insMdl{
  private:
    T dT;
    T Lat;
    T Long;
    T Height;
    float time;
    Matrix<T> PVA; // PVA vector
    Matrix<T> r; // Position vector
    Matrix<T> v; // Velocity vector
    Matrix<T> a; // Attitude vector
    Matrix<T> Wnb; // Inertial measurements in N-frame
    Matrix<T> fb; // Strap down acceleration measurements
    Matrix<T> Wie; // Rotation rate of the Earth w.r.t I-frame
    Matrix<T> Wen; // Transport rate of n-frame
    Matrix<T> DCM; // 3x3 Direction Cosine Matrix
    Matrix<T> Wib; // Strap down inertial measurements
    Matrix<T> Vdot; // Linear dynamics correction
    Matrix<T> gl; // plumb-bob gravity vector
    Quaternion<T> Q; // Reference Quaternion

  public:
    insMdl(); // sample time as argument, default 1s
    ~insMdl();
    void init();
    void getIMUStream(std::istream &s); // reads stream from a Specified file location
    void getCamStream(std::istream &s); // reads stream from a Specified file location
    void propagate(); // Returns propagated N-frame PVA vector
    void correct(); // Returns camera aided PVA vector
    template <class C>
      friend std::ostream &operator<<(std::ostream &s, const insMdl<C> &x);
};

template <class T>
insMdl<T>::insMdl(){
  /* FUNCTION: Constructs the insMdl class
   * PRE: Sample time must be specified 
   * POST: All vector dimensions are modified */
  
  // 100 Hz update
  dT=0.01;

  PVA.SetDimensions(1,9);
  r.SetDimensions(3,1);
  v.SetDimensions(3,1);
  a.SetDimensions(3,1);
  Wnb.SetDimensions(3,1);
  fb.SetDimensions(3,1);
  Wie.SetDimensions(3,1);
  Wen.SetDimensions(3,1);
  DCM.SetDimensions(3,3);
  Wib.SetDimensions(3,1);
  Vdot.SetDimensions(3,1);
  gl.SetDimensions(3,1);
}

template <class T>
insMdl<T>::~insMdl(){
}

template <class T>
void insMdl<T>::init(){
  /* FUNCTION: Initializes a Reference Quaternion, Earth Rotation
   * Rate and plump-bob gravity vector 
   * PRE: Gravity vector at stationary position from fb,
   * newest 6DOF reading.
   * POST: Quaternion Q initialized to N-frame using the gravity vector
   * STATUS: INCOMPLETE. Unsure of plump-bob gravity vector. */ 

  // Position of nav-frame 
  // Initial values taken from 
  // EEB835 Assignment 2, Sem2, 2008
  Lat = -0.478417;
  Long = 2.67246;
  // height of nav frame in metres
  Height = 300;

  // Initial position in NED frame
  r(0) = Lat;
  r(1) = Long;
  r(2) = Height;

  v(0) = 30.6;
  v(1) = -12.7;
  v(2) = -10.54;

  Wib(0) = 0;
  Wib(1) = 0;
  Wib(2) = 5.840735;

  // Translate initial Euler angles as reference quaternion
  Q.euler2quat(Wib(0), Wib(1), Wib(2));
  Q.normalize();
}

template <class T>
void insMdl<T>::getIMUStream(std::istream &s){
  /* FUNCTION: Grabs the inertial data from the std::istream 
   * and stores into memory 
   * PRE: std:istream &s must be formatted with exact
   * parameters specified in requirement XX.YYY.ZZ
   * POST: Current Matrix vectors fb, Wib are modified to
   * the new values extracted from the std::istream 
   * STATUS: INCOMPLETE. No error checking for unreadable
   * file.Extraction from std::istream not done yet.
   * Using dummy variables */

  //ignore number of chars that time is displayed
  //s.ignore(13);

  s >> time;
  s >> fb(0);
  s >> fb(1);
  s >> fb(2);
  s >> Wib(0);
  s >> Wib(1);
  s >> Wib(2);

  // ignore characters until end of line
  while(s.get()!='\n');
}

template <class T>
void insMdl<T>::getCamStream(std::istream &s){
  /* FUNCTION: Grabs the Visual data from the std::istream 
   * and stores into memory 
   * PRE: std:istream &s must be formatted with exact
   * parameters specified in requirement XX.YYY.ZZ
   * POST: Current Matrix vectors fb, Wib are modified to
   * the new values extracted from the std::istream 
   * STATUS: INCOMPLETE. No error checking for unreadable
   * file.Extraction from std::istream not done yet. */

  //ignore number of chars that time is displayed
  //s.ignore(13);
  s >> fb(0);

  s >> fb(0);
  s >> fb(1);
  s >> fb(2);
  s >> Wib(0);
  s >> Wib(1);
  s >> Wib(2);

  // ignore characters until end of line
  while(s.get()!='\n');
}

template <class T>
void insMdl<T>::propagate(){
  /* FUNCTION: Propagates the Position, Velocity and Attitude and
   * stores into the PVA Matrix type vector 
   * PRE: Requires the newest inertial data 
   * POST: Matrix vectors r, v, a are modified to
   * the new propagated data. PVA shall contain all
   * elements of r, v and a.
   * STATUS: COMPLETE */

  // constants can be implemented better
  T Omega = 15.04107/3600*PI/180;
  T R0 = 6371e3;

  //Create DCM of reference quaternion before any other operations
  DCM = Q.quat2DCM();

  // Update Earth rotation rate w.r.t inertial frame
  Wie(0) = Omega*cos(Lat);
  Wie(1) = 0;
  Wie(2) = -Omega*sin(Lat);

  // Update Transport rate. NED mechanisation
  Wen(0) = v(1)/(R0+Height);
  Wen(1) = -v(0)/(R0+Height);
  Wen(2) = -v(1)*tan(Lat)/(R0+Height);

  // Translate MEMS rates to N-Frame
  Wnb = Wib - DCM.Transpose()*(Wie+Wen);

  // Conversion to Rotation Quaternion P =[0,wx,wy,wz]
  Quaternion<T> P(0, Wnb(0), Wnb(1), Wnb(2));

  //Propogate and Normalize Quaternion
  Q += Q*P*0.5*dT;
  a = Q.quat2euler();

  // Plump-bob gravity vector m/s^2
  gl(0) = -0.5*pow(Omega,2)*(R0+Height)*sin(2*Lat);
  gl(1) = 0;
  gl(2) = 9.79-0.5*pow(Omega,2)*(R0+Height)*(1+cos(2*Lat));

  Matrix<T> Wie_p_Wen(3,1);
  Wie_p_Wen = Wie*2 + Wen;
  Matrix<T> coriolis(3,3);
  coriolis(0,0) = 0;
  coriolis(1,1) = 0;
  coriolis(2,2) = 0;
  coriolis(0,1) = -Wie_p_Wen(2);
  coriolis(0,2) = Wie_p_Wen(1);
  coriolis(1,2) = -Wie_p_Wen(0);
  coriolis(1,0) = -coriolis(0,1);
  coriolis(2,0) = -coriolis(0,2);
  coriolis(2,1) = -coriolis(1,2);
  
  // propagate velocity vector
  Vdot = DCM*fb - coriolis*v + gl; 
  v += Vdot*dT;

  // Propagate position. NED mechanisation
  Long += v(1)/((R0+Height)*cos(Lat))*dT;
  Lat += v(0)/(R0+Height)*dT;
  Height += -v(2)*dT;

  // Position defined in NED frame
  r(0) = Lat;
  r(1) = Long;
  r(2) = Height;

  // return the N-frame PVA vector
  PVA(0,0) = r(0);
  PVA(0,1) = r(1);
  PVA(0,2) = r(2);
  PVA(0,3) = v(0);
  PVA(0,4) = v(1);
  PVA(0,5) = v(2);
  PVA(0,6) = a(0);
  PVA(0,7) = a(1);
  PVA(0,8) = a(2);
}

template <class T>
void insMdl<T>::correct(){
  /* FUNCTION: Re-initializes the attitude to the
   * visual data measurements. 
   * PRE: Visual data available
   * POST: Reference quaternion is reset to the new
   * visual data attitude
   * DEPENDANTS: getCamStream()
   * STATUS: INCOMPLETE! Unsure of if Wib or Wnb */

  //getCamStream();

  // Re-initialize reference quaternion with new Euler Angles
  Q.euler2quat(Wib(0), Wib(1), Wib(2));
  Q.normalize();
}

template <class C>
std::ostream &operator<<(std::ostream &s, const insMdl<C> &x){
  s << x.PVA;
  return s;
}

#endif
