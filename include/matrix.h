// matrix.h
// Template Matrix library for use throughout AHNS Systems
//
// Steven Chen
//
// 2008

#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <stdexcept>
#include <iostream>
#include <string>
#include <math.h>

// Matrix Class

class BaseMatrix
{
 public:
  // Constructors
  BaseMatrix() : m_nRows(0), m_nCols(0) {};
  // Destructor
  virtual ~BaseMatrix() {};

  // Accessors
  virtual int GetRows() const { return m_nRows; };
  virtual int GetCols() const { return m_nCols; };

 protected:
  int m_nRows, m_nCols;
};

template <class T>
class Matrix : public BaseMatrix
{
 public:
  // Constructors
  Matrix();
  Matrix(const Matrix &srcMat); // Copy Constructor
  Matrix(int nSqrDimension, T rScalar = 0.0); // may encounter clashes with other constructor
  Matrix(int nRows, int nCols);
  Matrix(T *srcArray, int nRows, int nCols);

  // Add more overloads if required

  // Destructor
  ~Matrix();

  // Operators
  virtual Matrix &operator=(const Matrix <T> &m2);
  virtual Matrix &operator+=(const Matrix <T> &m2);
  virtual Matrix &operator-=(const Matrix <T> &m2);
  virtual Matrix operator+(const Matrix <T> &m2);
  virtual Matrix operator-(const Matrix <T> &m2);
  virtual Matrix operator*(const Matrix <T> &m2);
  virtual Matrix operator*(T rScalar); // Only want same type to smultiply matrix?

  virtual T &operator()(int r, int c); // Used as an element Accessor.
  virtual T &operator()(int r); // Used as a vector element Accessor.

  virtual Matrix Transpose();
  virtual Matrix Inverse();
  virtual T dot(const Matrix <T> &m2); // Dot product
  virtual Matrix cross(const Matrix <T> &m2); // Cross product

  // Maintenence Functions
  virtual void SetDimensions(int nRows, int Cols);

  template <class S>
    friend std::ostream &operator<<(std::ostream &s, const Matrix <S> &m);
  /*   template <T> */
  /*     friend std::istream &operator>>(std::istream &s, Matrix &m); */

 protected:
  T **m_ppData;
};

template <class S>
std::ostream &operator<<(std::ostream &s, const Matrix <S> &m);
/* template <class Q> */
/* std::istream &operator>>(std::istream &s, Matrix &m); */


  template <class T>
Matrix <T>::Matrix()
{
  m_nRows = 0;
  m_nCols = 0;
}

  template <class T>
Matrix <T>::Matrix(const Matrix &srcMat)
{
  int i, j;

  // Set number of rows and columns to be the same as the source
  SetDimensions(srcMat.m_nRows, srcMat.m_nCols);

  // Set each element to be the same as the source
  for (i=0; i<m_nRows; i++) {
    for(j=0; i<m_nCols; j++) {
      m_ppData[i][j] = srcMat.m_ppData[i][j];
    }
  }
}

template <class T>
Matrix <T>::Matrix(int nSqrDimension, T rScalar)
{
  /* SIDE EFFECT: Cannot create a template of integer
   * matrices since it clashes with row-column constructor
   * which also takes 2 integer arguments. */

  if (nSqrDimension < 1)
    throw "Error: Invalid dimensions";

  m_nRows = nSqrDimension;
  m_nCols = nSqrDimension;	
  m_ppData = new T*[m_nRows];

  try{
    if (m_ppData != NULL){
      for (int r = 0; r < m_nRows; r++){
	m_ppData[r] = new T[m_nCols];
      }
    }

    for (int r = 0; r < m_nRows; r++){
      m_ppData[r][r] = rScalar;
      for (int c = r+1; c < m_nCols; c++){
	m_ppData[r][c]=0;
	m_ppData[c][r]=0;
      }
    }
  }
  catch(.../*bad_alloc*/){
    delete m_ppData;
    m_ppData = NULL;
    //cout << "Error: not enough memory";
  }
}

  template <class T>
Matrix <T>::Matrix(int nRows, int nCols)
{
  int i;
  int j;

  SetDimensions(nRows, nCols);

  for (i=0; i<nRows; i++) {
    for (j=0; j <nCols; j++) {
      m_ppData[i][j] = 0.0;
    }
  }
  //printf("farking");
}

  template <class T>
Matrix <T>::Matrix(T *srcArray, int nRows, int nCols)
{
  int i,j,k = 0;

  m_nRows = nRows;
  m_nCols = nCols;

  for (i = 0; i < m_nRows; i++) {
    for (j = 0; j < m_nCols; j++) {
      m_ppData[i][j] = srcArray[k];
      k++;
    }
  }
}

  template <class T>
Matrix <T>::~Matrix()
{
  for(int i = 0; i < m_nRows; i++) {
    delete [] m_ppData[i];
  }
  delete [] m_ppData;
}

  template <class T>
void Matrix <T>::SetDimensions(int nRows, int nCols)
{
  int i;
  int j;

  if ((nRows <= 0)||(nCols <= 0)) {
    //    throw dim_error(); // NEEEEEEEEED CATCH???????????
  } else { 
    m_nRows = nRows;
    m_nCols = nCols;

    m_ppData = new T *[nRows];
    for (i=0;i<nRows;i++)
    {
      m_ppData[i] = new T [nCols];
    }
  }
}

using namespace std;

  template <class T>
Matrix <T> &Matrix <T>::operator=(const Matrix <T> &m2)
{
  int i;
  int j;

  if ((m_nRows != m2.GetRows()) || (m_nCols != m2.GetCols())) {
    for(i=0; i<m_nRows; i++)
    {
      delete [] m_ppData[i];
    }
    delete [] m_ppData;
    SetDimensions(m2.GetRows(),m2.GetCols());
  }
  for (i=0; i<m_nRows; i++) {
    for (j=0; j<m_nCols; j++) {
      m_ppData[i][j] = m2.m_ppData[i][j];
    }
  }
  return (*this);
}

  template <class T>
Matrix <T> &Matrix <T>::operator+=(const Matrix <T> &m2)
{
  if ((m_nRows != m2.m_nRows)||(m_nCols != m2.m_nCols))
    throw range_error("Dimension mismatch error!");

  for(int i=0; i<m_nRows; i++)
  {
    for (int j=0; j <m_nCols; j++)
    {
      m_ppData[i][j] += m2.m_ppData[i][j];
    } 

  }
  return *this;
}

  template <class T>
Matrix <T> &Matrix <T>::operator-=(const Matrix <T> &m2)
{
  if ((m_nRows != m2.m_nRows)||(m_nCols != m2.m_nCols))
    throw range_error("Dimension mismatch error!");

  for(int i=0; i<m_nRows; i++)
  {
    for (int j=0; j <m_nCols; j++)
    {
      m_ppData[i][j] -= m2.m_ppData[i][j];
    } 

  }
  return *this;
}

  template <class T>
Matrix <T> Matrix <T>::operator+(const Matrix <T> &m2)
{
  Matrix <T> temp(m_nRows,m_nCols);
  if ((m_nRows != m2.m_nRows)||(m_nCols != m2.m_nCols))
  {
    //     throw mismatch_error();
    //return (*this);
  }
  else
  {
    for(int i=0; i<m_nRows; i++)
    {
      for (int j=0; j <m_nCols; j++)
      {
	temp.m_ppData[i][j] = (m_ppData[i][j]) + (m2.m_ppData[i][j]);
      } 

    }
    return temp;
  }
}

  template <class T>
Matrix <T> Matrix <T>::operator-(const Matrix <T> &m2)
{
  Matrix <T> temp(m_nRows,m_nCols);
  if ((m_nRows != m2.m_nRows)||(m_nCols != m2.m_nCols))
  {
    //     throw mismatch_error();
    //return (*this);
  }
  else
  {
    for(int i=0; i<m_nRows; i++)
    {
      for (int j=0; j <m_nCols; j++)
      {
	temp.m_ppData[i][j] = (m_ppData[i][j]) - (m2.m_ppData[i][j]);
      } 

    }
    return temp;
  }
}

  template <class T>
Matrix <T> Matrix <T>::operator*(const Matrix <T> &m2)
{
  if (m_nCols != m2.m_nRows)
    throw "dimension mismatch!";

  Matrix <T> temp(m_nRows,m2.m_nCols);

  for(int i=0; i<m_nRows; i++)
  {
    for(int j=0; j<m2.m_nCols; j++)
    {
      temp.m_ppData[i][j] = 0;
      for(int k=0; k<m_nCols; k++)
      {
	temp.m_ppData[i][j]+= this->m_ppData[i][k] * m2.m_ppData[k][j];
      }
    } 
  }
  return temp;
}

  template <class T>
Matrix <T> Matrix <T>::operator*(T rScalar)
{
  Matrix <T> temp(m_nRows,m_nCols);

  for(int i=0; i< m_nRows; i++)
  {
    for (int j=0; j < m_nCols; j++)
    {
      temp.m_ppData[i][j] = rScalar*this->m_ppData[i][j];
    }
  }
  return temp;
}

  template <class T>
Matrix <T> Matrix <T>::Transpose()
{
  int i, j;

  Matrix <T> temp(m_nCols, m_nRows);

  for (i = 0; i<m_nRows; i++) {
    for (j = 0; j < m_nCols; j++) {
      temp(j,i) = m_ppData[i][j];
    }
  }  

  return (temp);
}

template <class T>
Matrix <T> Matrix<T>::Inverse()
{ 
  /* Function: Calculates the Inverse of a square matrix.
   * PRE: Invoking matrix must be square.
   * POST: Invoking matrix remains unchanged.
   * INPUTS: Object matrix.
   * OUTPUTS: Matrix inverse return of invoking matrix.
   * Throw error if the matrix is not square dimensional.
   * Throw error if matrix is non-invertible.
   * DEPENDANTS: Copy constructor, scalar constructor,
   * accessor operator.
   * SIDE EFFECTS: Cannot calculate matrix with zero column. */
  std::cout << "doesnt work!!!!!!!";

  if (m_nRows != m_nCols)
    throw "Dimension mismatch: matrix must be square dimensional";

  Matrix <T> mat(*this);
  Matrix <T> bb(m_nRows, (T)1.0);

  int *pvt = new int [m_nRows];                      // pivoting vector 
  for (int k = 0; k < m_nRows; k++) pvt[k] = k;

  T *scale  = new T[m_nRows];             // find scale vector
  for (int k = 0; k < m_nRows; k++) {
    scale[k] = 0;
    for (int j = 0; j < m_nRows; j++) 
      if (fabs(scale[k]) < fabs(mat(k,j))) scale[k] = fabs(mat(k,j));
  } 

  for (int k = 0; k < m_nRows-1; k++) {            // main elimination loop

    // find the pivot in column k in rows pvt[k], pvt[k+1], ..., pvt[n-1]
    int pc = k; 
    T largest_pivot = fabs(mat(pvt[k],k)/scale[k]);
    for (int i = k+1; i < m_nRows; i++) {
      T tmp = fabs(mat(pvt[i],k)/scale[pvt[i]]); 
      if (tmp > largest_pivot) {
	largest_pivot = tmp; 
	pc = i;
      }
    }
    if (largest_pivot == 0)
      throw "error: Matrix is non-invertible!";

    if (pc != k) {                              // swap pvt[k] and pvt[pc]
      int ii = pvt[k];
      pvt[k] = pvt[pc];
      pvt[pc] = ii;
    }

    // now eliminate the column entries logically below mat(pvt[k],k)
    int pvtk = pvt[k];                           // pivot row
    for (int i = k+1; i < m_nRows; i++) {
      int pvti = pvt[i];
      if (mat(pvti,k) != 0) {
	T mult = mat(pvti,k)/mat(pvtk,k); 
	mat(pvti,k) = mult;
	for (int j = k+1; j < m_nRows; j++) mat(pvti,j) -= mult*mat(pvtk,j);
      }
    }
  }

  // Solution matrix
  Matrix <T> xx(m_nRows,m_nCols);
  for(int n=0; n < m_nCols; n++){
    // forward substitution for L y = Pb. Store y in b
    for (int i = 1; i < m_nRows; i++)  
      for (int j = 0; j < i; j++) bb(pvt[i],n) -= mat(pvt[i],j)*bb(pvt[j],n);

    // back substitution  for Ux = y
    for (int i = m_nRows - 1; i >= 0; i--) {
      for (int j = i+1; j < m_nRows; j++) bb(pvt[i],n) -= mat(pvt[i],j)*xx(j,n);
      xx(i,n) = bb(pvt[i],n) / mat(pvt[i],i);
    }
  }
  return xx;
}

template <class T>
T Matrix <T>::dot(const Matrix <T> &m2){
  /* DESCRIPTION: Performs the inner dot product of two vectors.
   * INPUTS: Invoking Matrix, 2nd Matrix. 
   * OUTPUTS: A scalar of type "T".
   * PRE CONDITION: Both matrix dimensions must be nx1
   * POST CONDITION: Both matrix objects remain unaltered.
   * SIDE EFFECTS: None. */

  if((m_nCols != 1) || (m2.m_nCols != 1))
    throw "Invalid vector(s), matrix must only consist of one column";

  else if(m_nRows != m2.m_nRows)
    throw "Vector dimension mismatch";

  T temp = 0;
  for(int r = 0; r < m_nRows; r++)
    temp += m_ppData[r][0] * m2.m_ppData[r][0];

  return temp;
}

template <class T>
Matrix <T> Matrix <T>::cross(const Matrix <T> &m2){
  /* DESCRIPTION: Performs the cross product of 2
   * 3-dimensional vectors.
   * INPUTS: Invoking Matrix, 2nd Matrix.
   * OUTPUTS: Matrix in 3x1-dimensions.
   * PRE CONDITION: Both matrices must only have 3x1 dimensions.
   * POST CONDITION: Both matrix objects remain unaltered.
   * SIDE EFFECTS: None. */

  if((m_nCols != 1) || (m2.m_nCols != 1))
    throw "Invalid vector(s), matrix must only consist of one column";

  else if((m_nRows != 3) || (m2.m_nRows != 3))
    throw "cannot perform cross product. 3 dim only!";

  Matrix <T> temp(*this);

  temp.m_ppData[0][0] = m_ppData[1][0]*m2.m_ppData[2][0] - m_ppData[2][0]*m2.m_ppData[1][0];
  temp.m_ppData[1][0] = m_ppData[2][0]*m2.m_ppData[0][0] - m_ppData[0][0]*m2.m_ppData[2][0];
  temp.m_ppData[2][0] = m_ppData[0][0]*m2.m_ppData[1][0] - m_ppData[1][0]*m2.m_ppData[0][0];

  return temp;
}



  template <class T>
T &Matrix <T>::operator()(int r, int c)
{
  // do commenting!
  T *p;

  if ((r <= -1) || (r >= (m_nRows)) || (c <= -1) || (c >= (m_nCols))) 
    throw range_error("Vector index access out of bounds");

  else 
    p = &m_ppData[r][c];

  return (*p);
}

  template <class T>
T &Matrix <T>::operator()(int r)
{
  if(m_nCols != 1)
    throw domain_error("Invalid vector, matrix must only consist of one column");

  if((r < 0) || (r > m_nRows-1))
    throw range_error("Vector index access out of bounds");

  else return m_ppData[r][0];
}

  template <class S>
std::ostream &operator<<(std::ostream &s, const Matrix <S> &m)
{
  int i;
  int j;

  s.precision(8);
  for (i = 0; i < m.GetRows(); i++) {
    for (j = 0; j < m.GetCols(); j++) {
      s << m.m_ppData[i][j]<< " ";
    }
    s << endl;
  }  
  return (s);
}

/*
   template <class Q>
   std::istream &operator>>(std::istream &s, Matrix <Q> &m)
   {
   }*/


#endif
