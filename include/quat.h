/* Quaternion class - made by Neville Tummon */

#ifndef _QUATERNION_H
#define _QUATERNION_H

#include <iostream>
#include "matrix.h"
#include <math.h>
#include <iomanip>

#define PI 3.14159265
using namespace std;

template <class T>
class Quaternion
{

  public:

    Quaternion();
    Quaternion(T r, T i, T j, T k);
    Quaternion(const Quaternion &srcquat); /* copy constructor */
    ~Quaternion();

    //operators
    virtual Quaternion& operator=(const Quaternion &q2);
    virtual Quaternion& operator+=(const Quaternion &q2);
    virtual Quaternion& operator-=(const Quaternion &q2);
    virtual Quaternion& operator*=(const Quaternion &q2);
    virtual Quaternion operator+(const Quaternion &q2);
    virtual Quaternion operator-(const Quaternion &q2);
    virtual Quaternion operator*(const Quaternion &q2);
    virtual Quaternion operator*(T rScalar);

    //member functions
    virtual T abs(); /* calculate length of Quaternion */
    virtual void rotate(Quaternion &rotQuat); /* Q becomes new rotated Q */
    virtual void normalize();
    virtual Quaternion conj(); /* Q=[a,v] to Q'=[a,-v] */
    virtual void euler2quat(T roll, T pitch, T yaw);
    virtual Matrix<T> quat2euler(); // must include matrix.h
    virtual Matrix<T> quat2DCM(); // must include matrix.h

    /* accessors */
    virtual T setA(T W){ a = W; };
    virtual T setB(T X){ b = X; };
    virtual T setC(T Y){ c = Y; };
    virtual T setD(T Z){ d = Z; };

    /* returns the DCM of the quaternion q */
    template <class C>
      friend std::ostream &operator<<(std::ostream &s, const Quaternion<C> &q);
    template <class C>
      friend Quaternion<C> operator*(C rScalar, Quaternion<C> &q2);
    //template <class C>
    //friend Quaternion<C> operator*(C rScalar);

  protected:

    T a, b, c, d;

};

//template <class C>
//Quaternion<C> operator*(C rScalar, Quaternion<C> &q2);

template <class C>
std::ostream &operator<<(std::ostream &s, const Quaternion<C> &q);

//////////////////////////////////////////////////////////

template <class C>
Quaternion<C> operator*(C rScalar, Quaternion<C> &q2){

  q2.a *= rScalar;
  q2.b *= rScalar;
  q2.c *= rScalar;
  q2.d *= rScalar;
}

template <class C>
std::ostream &operator<<(std::ostream &s, const Quaternion<C> &q){
  s.precision(3);
  s << fixed << q.a << " " << q.b << " " << q.c << " " << q.d;
  return s;
}

template <class T>
Quaternion<T>::Quaternion(){
  a = 1.0;
  b = 0.0;
  c = 0.0;
  d = 0.0;
}

template <class T>
Quaternion<T>::Quaternion(T r, T i, T j, T k){
  a = r;
  b = i;
  c = j;
  d = k;
}

template <class T>
Quaternion<T>::~Quaternion(){

}

template <class T>
Quaternion<T>::Quaternion(const Quaternion &srcquat){
  a = srcquat.a;
  b = srcquat.b;
  c = srcquat.c;
  d = srcquat.d;
}

template <class T>
T Quaternion<T>::abs(){
  return sqrt(a*a + b*b + c*c + d*d);
}

template <class T>
void Quaternion<T>::rotate(Quaternion &rotQuat){
  /* Function: Rotates a position quaternion about a 
   * rotational Quaternion.
   * PRE: Original position quaternion parameters set
   * POST: Original quaternion changed to rotated quaternion
   * DEPENDANTS: operator*, normalize(), copy constructor
   * STATUS: Performance comparisons against rotate2().
   * Rotation accuracy PASS */

  Quaternion *Qk(this);
  /* only normalize if square error is above 0.02^2 */
  if ((rotQuat.abs()-1)*(rotQuat.abs()-1) > 0.02*0.02){
    rotQuat.normalize();
  }
  else;
  *Qk = rotQuat*(*Qk)*rotQuat.conj();
  /* only normalize if square error is above 0.01^2 */
  if ((Qk->abs()-1)*(Qk->abs()-1) > 0.001*0.001){
    Qk->normalize();
  }
  else;
}

template <class T>
Quaternion <T> Quaternion<T>::conj(){
  /* Function: Quaternion conjugate
   * PRE: Object quaternion passed into function
   * POST: The quaternion conjugate result is returned
   * STATUS: complete */

  Quaternion<T> Qt;
  Qt.a = a;
  Qt.b = -b;
  Qt.c = -c;
  Qt.d = -d;
  return Qt;
}

template <class T>
void Quaternion<T>::normalize(){
  /* Function: Normalizes the Quaternion to unity length
   * PRE: Object quaternion passed into function
   * POST: Each element of the Quaternion is divided
   * by the lenght of the Quaternion vector.
   * DEPENDANTS: abs()
   * STATUS: complete */
  T L = abs();
  a /= L;
  b /= L;
  c /= L;
  d /= L;
}

template <class T>
Quaternion<T> Quaternion<T>::operator+(const Quaternion &q2){
  Quaternion<T> q1;

  q1.a = a + q2.a;
  q1.b = b + q2.b;
  q1.c = c + q2.c;
  q1.d = d + q2.d;

  return q1;
}

template <class T>
Quaternion<T> &Quaternion<T>::operator+=(const Quaternion &q2){

  a += q2.a;
  b += q2.b;
  c += q2.c;
  d += q2.d;

  return *this;
}

template <class T>
Quaternion<T> Quaternion<T>::operator-(const Quaternion &q2){
  Quaternion<T> q1;

  q1.a = a - q2.a;
  q1.b = b - q2.b;
  q1.c = c - q2.c;
  q1.d = d - q2.d;

  return q1;
}

template <class T>
Quaternion<T> &Quaternion<T>::operator-=(const Quaternion &q2){

  a -= q2.a;
  b -= q2.b;
  c -= q2.c;
  d -= q2.d;

  return *this;
}

template <class T>
Quaternion<T> &Quaternion<T>::operator=(const Quaternion &q2){
  a = q2.a;
  b = q2.b;
  c = q2.c;
  d = q2.d;
}

template <class T>
Quaternion<T> Quaternion<T>::operator*(const Quaternion &q2){
  Quaternion<T> q1;

  q1.a = a*q2.a - b*q2.b - c*q2.c - d*q2.d;
  q1.b = b*q2.a + a*q2.b - d*q2.c + c*q2.d;
  q1.c = c*q2.a + d*q2.b + a*q2.c - b*q2.d;
  q1.d = d*q2.a - c*q2.b + b*q2.c + a*q2.d;

  return q1;
}

template <class T>
Quaternion<T> &Quaternion<T>::operator*=(const Quaternion &q2){
  Quaternion<T> q1(*this);

  a = q1.a*q2.a - q1.b*q2.b - q1.c*q2.c - q1.d*q2.d;
  b = q1.b*q2.a + q1.a*q2.b - q1.d*q2.c + q1.c*q2.d;
  c = q1.c*q2.a + q1.d*q2.b + q1.a*q2.c - q1.b*q2.d;
  d = q1.d*q2.a - q1.c*q2.b + q1.b*q2.c + q1.a*q2.d;

  return *this;
}

template <class T>
Quaternion<T> Quaternion<T>::operator*(T rScalar)
{
  Quaternion<T> q1;

  q1.a = a*rScalar;
  q1.b = b*rScalar;
  q1.c = c*rScalar;
  q1.d = d*rScalar;

  return q1;
}

/*template <class T>
  Quaternion<T> Quaternion<T>::operator*(T rScalar){

  a *= rScalar;
  b *= rScalar;
  c *= rScalar;
  d *= rScalar;
  }*/

template <class T>
void Quaternion<T>::euler2quat(T roll, T pitch, T yaw){
  /* Function: Converts euler pitch, yaw and roll angles to quaternion form 
   * PRE: Initialized object quaternion, pitch, yaw and roll parameters
   * expressed in radians.
   * POST: Object quaternion changed to rotation quaternion
   * STATUS: Complete */

  // Euler angles expressed in quaternions, Reference from Titterton
  a = cos(roll/2)*cos(pitch/2)*cos(yaw/2)+sin(roll/2)*sin(pitch/2)*sin(yaw/2);
  b = sin(roll/2)*cos(pitch/2)*cos(yaw/2)-cos(roll/2)*sin(pitch/2)*sin(yaw/2);
  c = cos(roll/2)*sin(pitch/2)*cos(yaw/2)+sin(roll/2)*cos(pitch/2)*sin(yaw/2);
  d = cos(roll/2)*cos(pitch/2)*sin(yaw/2)+sin(roll/2)*sin(pitch/2)*cos(yaw/2);
}

template <class T>
Matrix<T> Quaternion<T>::quat2euler(){
  /* Function: Converts quaternion to Euler roll, pitch, yaw respectively
   * and described in degrees.
   * PRE: Initialized object quaternion
   * POST: Returns a 3x1 matrix (vector) containing roll, pitch and yaw
   * STATUS: Unsure if in r p c order. Degrees/Radians?? */

  Matrix<T> cache(3,1);
  // Equations from Titterton reference
  cache(0,0) = atan2(2*(a*b + c*d) , a*a - b*b - c*c + d*d);
  cache(1,0) = asin(2*(a*c - b*d));
  cache(2,0) = atan2(2*(b*c + a*d) , a*a + b*b - c*c - d*d);

  if(cache(2,0) < 0)
    cache(2,0) += 2*PI;

  return cache;
}

template <class T>
Matrix<T> Quaternion<T>::quat2DCM(){

  Matrix<T> cache(3,3);
  cache(0,0) = a*a + b*b - c*c - d*d;
  cache(0,1) = 2*(b*c - a*d);
  cache(0,2) = 2*(b*d + a*c);

  cache(1,0) = 2*(b*c + a*d);
  cache(1,1) = a*a - b*b + c*c - d*d; 
  cache(1,2) = 2*(c*d - a*b);

  cache(2,0) = 2*(b*d - a*c);
  cache(2,1) = 2*(c*d + a*b);
  cache(2,2) = a*a - b*b - c*c + d*d;

  return cache;
}

#endif
