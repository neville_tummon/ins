
#include "matrix.h"

template <class T>
class TVector : public Matrix<T>
{
  public:
    TVector();
    TVector(int nRows);
    TVector(T *srcArray, int nRows);
    TVector(const TVector &srcVec);
    ~TVector();

    virtual T& operator()(int r); // accessor
    virtual TVector& operator+=(const TVector &v2);
    virtual TVector& operator-=(const TVector &v2);
    virtual TVector operator+(const TVector &v2);
    virtual TVector operator-(const TVector &v2);
    virtual TVector& operator=(const TVector &v2);
    virtual T operator*(const TVector &v2); // dot product
    virtual TVector operator%(const TVector &v2); // cross product
    virtual void SetDimensions(int nRows);
};

template <class T>
TVector<T>::TVector() {}

  template <class T>
TVector<T>::TVector(int nRows) : Matrix<T>(nRows, 1)
{

}

template <class T>
TVector<T>::TVector(T *srcArray, int nRows) : Matrix<T>(nRows,1)
{
  for(int r = 0; r < this->m_nRows; r++)
    this->m_ppData[r][0] = srcArray[r];
}

template <class T>
TVector<T>::TVector(const TVector &srcVec) : Matrix<T>(srcVec) {}

template <class T>
TVector<T>::~TVector() {}

template <class T>
T& TVector<T>::operator()(int r)
{ 
  if((r < 0) || (r > this->m_nRows))
    throw range_error("Vector index access out of bounds");

  else return this->m_ppData[r][0];
}

template <class T>
T TVector<T>::operator*(const TVector &v2)
{
  /* DESCRIPTION: Performs the inner dot product of two
   * CDblVectors.
   * INPUTS: Invoking TVector, 2nd TVector. 
   * OUTPUTS: A scalar of type "double".
   * PRE CONDITION: Both vector dimensions must be
   * of equal length.
   * POST CONDITION: Both vector objects remain unaltered.
   * SIDE EFFECTS: None. */

  if(this->m_nRows != v2.m_nRows)
    throw "Vector dimension mismatch";

  double temp = 0;
  for(int r = 0; r < this->m_nRows; r++)
    temp += this->m_ppData[r][0] * v2.m_ppData[r][0];

  return temp;
}

template <class T>
TVector<T>& TVector<T>::operator=(const TVector &v2)
{
  if(this->m_nRows != v2.m_nRows)
    throw range_error("Vector dimension mismatch error!");

  for(int r = 0; r < this->m_nRows; r++)
    this->m_ppData[r][0] = v2.m_ppData[r][0];

  return *this;
}

template <class T>
TVector<T> TVector<T>::operator+(const TVector &v2)
{
  if(this->m_nRows != v2.m_nRows)
    throw range_error("Vector dimension mismatch error!");

  TVector temp(*this);

  for(int r = 0; r < this->m_nRows; r++)
    temp.m_ppData[r][0] += v2.m_ppData[r][0];

  return temp;
}

template <class T>
TVector<T>& TVector<T>::operator+=(const TVector &v2)
{
  if(this->m_nRows != v2.m_nRows)
    throw range_error("Vector dimension mismatch error!");

  for(int r = 0; r < this->m_nRows; r++)
    this->m_ppData[r][0] += v2.m_ppData[r][0];

  return *this;
}

template <class T>
TVector<T> TVector<T>::operator-(const TVector &v2)
{
  if(this->m_nRows != v2.m_nRows)
    throw range_error("Vector dimension mismatch error!");

  TVector temp(*this);

  for(int r = 0; r < this->m_nRows; r++)
    temp.m_ppData[r][0] -= v2.m_ppData[r][0];

  return temp;
}

template <class T>
TVector<T>& TVector<T>::operator-=(const TVector &v2)
{
  if(this->m_nRows != v2.m_nRows)
    throw range_error("Vector dimension mismatch error!");

  for(int r = 0; r < this->m_nRows; r++)
    this->m_ppData[r][0] -= v2.m_ppData[r][0];

  return *this;
}

template <class T>
TVector<T> TVector<T>::operator%(const TVector &v2)
{
  /* DESCRIPTION: Performs the cross product of 2
   * 3-dimensional vectors.
   * INPUTS: Invoking TVector, 2nd TVector.
   * OUTPUTS: TVector in 3-dimensions.
   * PRE CONDITION: Both vectors must only have 3 dimensions.
   * POST CONDITION: Both vector objects remain unaltered.
   * SIDE EFFECTS: None. */

  if((this->m_nRows != 3) || (v2.m_nRows != 3))
    throw "cannot perform cross product. 3 dim only!";

  TVector temp(*this);

  temp.m_ppData[0][0] = this->m_ppData[1][0]*v2.m_ppData[2][0] - this->m_ppData[2][0]*v2.m_ppData[1][0];
  temp.m_ppData[1][0] = this->m_ppData[2][0]*v2.m_ppData[0][0] - this->m_ppData[0][0]*v2.m_ppData[2][0];
  temp.m_ppData[2][0] = this->m_ppData[0][0]*v2.m_ppData[1][0] - this->m_ppData[1][0]*v2.m_ppData[0][0];

  return temp;
}

  template <class T>
void TVector<T>::SetDimensions(int nRows)
{
  if (nRows <= 0)
    throw range_error("Invalid Dimension parameters!");

  this->m_nRows = nRows;
  this->m_nCols = 1;

  this->m_ppData = new T *[this->m_nRows];
  for (int i = 0; i < this->m_nRows; i++)
  {
    this->m_ppData[i] = new T [1];
  }
}

