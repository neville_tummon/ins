#include "ins.h"
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char *argv[])
{
  double var;
  std::ifstream file;
  file.open("IMU-OUTPUT.txt");

  std::ofstream dfFile;
  dfFile.open("DF-OUTPUT.txt");

  insMdl <float> INS;
  INS.getIMUStream(file);
  INS.init();

  for(int i=0; i<12000; i++){
    INS.getIMUStream(file);
    INS.propagate();
    cout << INS;
    dfFile << INS;
  }

  return EXIT_SUCCESS;
}
