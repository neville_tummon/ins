#include "quat.h"
#include "matrix.h"
#include <iostream>
#include <unistd.h>
#include <fstream>

using namespace std;

int main(int argc, char *argv[])
{
  // Constructors
  Quaternion<double> Quat;
  cout << "default Quaternion SUCCESS " << Quat << endl;
  //Quaternion<int> iQuat;
  //cout << "int Quaternion SUCCESS " << iQuat << endl;
  Quaternion<long> lQuat(4, 1, -6, 1);
  cout << "long Quaternion SUCCESS " << lQuat << endl;
  Quaternion<float> fQuat(2.1, 5.3, 0.7, -3.6);
  cout << "float Quaternion SUCCESS " << fQuat << endl;
  Quaternion<double> dQuat(222251.3, 551515.16, -3151627.63, 626261341.21);
  cout << "double Quaternion SUCCESS " << dQuat << endl;

  Quaternion<double> Quat01(1, 1, 1, 1);
  Quaternion<double> Quat02(1, -1, 0, 0);
  Quaternion<double> Quat03(0, 1, -1, 0);
  Quaternion<double> Quat04(0, 0, 1, -1) ;
  Quaternion<double> Quat05(-1, 0, 0, 1);
  Quaternion<double> Quat06(5, 2, 3, -4);
  Quaternion<double> Quat07(2, 6, -5, 8);
  Quaternion<double> Quat08(4.6, 2.2, 9.7, -6.5);
  Quaternion<double> Quat09(1.1, -4.3, -6.2, -1);

  Quaternion<double> Quat10(2, -7, 1, 4);
  Quaternion<double> Quat11(9, 2, 5, -10);
  Quaternion<double> Quat12(8.2, 4.0, 6.8, 6.9);
  Quaternion<double> Quat13(2.5, 0.3, 3.5, -1.5);

  Quaternion<double> Quat14(1, 2, 3, 5);
  Quaternion<double> Quat15(4, 0, 0, 0);
  Quaternion<double> Quat16(0, 4, 0, 0);
  Quaternion<double> Quat17(0, 0, 4, 0);
  Quaternion<double> Quat18(0, 0, 0, 4);
  Quaternion<double> Quat19(2, 4, 6, 8);
  Quaternion<double> Quat20(1, 3, 5, 7);
  Quaternion<double> Quat21(1.3, -2.4, 6.6, 2.4);
  Quaternion<double> Quat22(-8.8, -2.9, 6.7, 0.1);
  Quaternion<double> Quat23(-2.0, 5.1, -0.2, 0.4);
  Quaternion<double> Quat24(7.9, 1.4, 1.7, -3.2);
  Quaternion<double> Quat25;
  Quaternion<double> Quat26;
  Quaternion<double> Quat27;
  Quaternion<double> Quat28;
  Quaternion<double> Quat29;
  Quaternion<double> Quat30;
  Quaternion<double> Quat31;

  // Accessor tests
  Quat.setA(1.2);
  Quat.setB(3.4);
  Quat.setC(5.6);
  Quat.setD(7.8);
  cout << "Accessor Test: " << Quat << endl;

  cout << "Euler-Quaternion test" << endl;
  Quat25.euler2quat(PI,0,0);
  cout << "Quaternion " << Quat25 << endl;
  Quat26.euler2quat(0,PI,0);
  cout << "Quaternion " << Quat26 << endl;
  Quat27.euler2quat(0,0,PI);
  cout << "Quaternion " << Quat27 << endl;
  Quat28.euler2quat(PI/2,0,0);
  cout << "Quaternion " << Quat28 << endl;
  Quat29.euler2quat(0,PI/2,0);
  cout << "Quaternion " << Quat29 << endl;
  Quat30.euler2quat(0,0,PI/2);
  cout << "Quaternion " << Quat30 << endl;
  Quat31.euler2quat(PI/2,PI/2,PI/2);
  cout << "Quaternion " << Quat31 << endl;


  cout << "Quaternion-Euler test" << endl;
  cout << Quat25.quat2euler() << endl;
  cout << Quat26.quat2euler() << endl;
  cout << Quat27.quat2euler() << endl;
  cout << Quat28.quat2euler() << endl;
  cout << Quat29.quat2euler() << endl;
  cout << Quat30.quat2euler() << endl;
  cout << Quat31.quat2euler() << endl;

  cout << "Quaternion-DCM test" << endl;
  cout << "Quaternion " << Quat25 << endl;
  cout << Quat25.quat2DCM() << endl;
  cout << "Quaternion " << Quat26 << endl;
  cout << Quat26.quat2DCM() << endl;
  cout << "Quaternion " << Quat27 << endl;
  cout << Quat27.quat2DCM() << endl;
  cout << "Quaternion " << Quat28 << endl;
  cout << Quat28.quat2DCM() << endl;
  cout << "Quaternion " << Quat29 << endl;
  cout << Quat29.quat2DCM() << endl;
  cout << "Quaternion " << Quat30 << endl;
  cout << Quat30.quat2DCM() << endl;
  cout << "Quaternion " << Quat31 << endl;
  cout << Quat31.quat2DCM() << endl;

  cout << "addition operator" << endl;
  cout << Quat01 + Quat02 << endl;
  cout << Quat01 + Quat03 << endl;
  cout << Quat01 + Quat04 << endl;
  cout << Quat01 + Quat05 << endl;
  cout << Quat06 + Quat07 << endl;
  cout << Quat08 + Quat09 << endl << endl;

  cout << "subtraction operator" << endl;
  cout << Quat01 - Quat02 << endl;
  cout << Quat01 - Quat03 << endl;
  cout << Quat01 - Quat04 << endl;
  cout << Quat01 - Quat05 << endl;
  cout << Quat10 - Quat11 << endl;
  cout << Quat12 - Quat13 << endl << endl;

  // not finished
  cout << "multiplication operator" << endl;
  cout << Quat14 * Quat15 << endl;
  cout << Quat14 * Quat16 << endl;
  cout << Quat14 * Quat17 << endl;
  cout << Quat14 * Quat18 << endl;
  cout << Quat19 * Quat20 << endl;
  cout << Quat21 * Quat22 << endl;
  cout << Quat23 * Quat24 << endl << endl;

  cout << "scalar multiplication" << endl;
  cout << Quat19 * 0.5 << endl;
  cout << Quat19 * -4.2 << endl;
  cout << Quat19 * 3 << endl;
  cout << Quat19 * 0 << endl;
  return EXIT_SUCCESS;
}
