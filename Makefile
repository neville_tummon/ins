INCLUDE = -Iinclude/
CC = g++
CFLAGSL = -ansi -o
CFLAGSC = -c

insTestDriver: obj/insTestDriver.o
	$(CC) $(CFLAGSL) insTestDriver obj/insTestDriver.o

qTestDriver : obj/qTestDriver.o
	$(CC) $(CFLAGSL) qTestDriver obj/qTestDriver.o

matrixTestDriver : obj/matrixTestDriver.o
	$(CC) $(CFLAGSL) matrixTestDriver obj/matrixTestDriver.o

obj/insTestDriver.o: src/insTestDriver.cpp
	$(CC) $(CFLAGSC) src/insTestDriver.cpp -o obj/insTestDriver.o $(INCLUDE)

obj/qTestDriver.o: src/qTestDriver.cpp
	$(CC) $(CFLAGSC) src/qTestDriver.cpp -o obj/qTestDriver.o $(INCLUDE)

obj/matrixTestDriver.o: src/matrixTestDriver.cpp
	$(CC) $(CFLAGSC) src/matrixTestDriver.cpp -o obj/matrixTestDriver.o $(INCLUDE)
clean:
	rm -rf obj/*
	rm -rf bin/*
	rm -rf qTestDriver
	rm -rf insTestDriver
	rm -rf matrixTestDriver
	rm -rf DF-OUTPUT.txt
